Self-hosted Matrix Homeserver installation
==========================================

This repository contains the [Terraform](https://www.terraform.io/) code to provision a self-hosted [Matrix](https://matrix.org/) installation using
the [spantaleev/matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy) repository.


Requirements
------------
This has been tested with:

- Terraform version 1.1.3
- Ansible version 2.10.16 on Python 3.10.1
- matrix-docker-ansible-deploy commit [548d495d81d7197df25490424976c1571c7acc5d](https://github.com/spantaleev/matrix-docker-ansible-deploy/commit/548d495d81d7197df25490424976c1571c7acc5d).
- Provider [null](https://registry.terraform.io/providers/hashicorp/null/latest/docs) v3.1.0
- Provider [hcloud](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs) v1.32.2
- Provider [namecheap](https://registry.terraform.io/providers/namecheap/namecheap/latest/docs) v2.0.2

But it is likely to work on newer versions of these requirements as well.

The following service providers are used and valid API keys have to be provided for the services.

- Hetzner Cloud for the VPS to run the Matrix homeserver on.
- Namecheap for setting up the DNS records for the Matrix homeserver.

See `variables.tf` for the list of variables that can be customized.

Usage
-----
* Install Terraform.
* Clone this git repository locally.
* Clone the git submodule by running `git submodule update --init`.
* Copy `matrix-docker-ansible-deploy/examples/vars.yml` to `ansible-config/vars.yml`. Set up any Ansible variable overrides in this file. The `matrix_domain` variable will be automatically set by terraform.
* Create a file named `secrets.auto.tfvars` and specify the values for the variables defined in `variables.tf`. It is mandatory to specify the values for at least the following variables.
  * `hcloud_token`.
  * `namecheap_user_name`.
  * `namecheap_api_token`.
  * `namecheap_matrix_base_domain`.
  Alternatively, these can be set via environment variables as documented [here](https://www.terraform.io/cli/config/environment-variables#tf_var_name).
* Run `terraform init` to initialize the terraform project and download the providers.
* Run `terraform plan` to see the changes that will be applied by Terraform. Optionally, save the plan to a file.
* Run `terraform apply`. This should set up all the infrastructure resources and also run the Ansible playbook(s) needed to set up the Matrix homeserver.
* Set up Matrix service discovery by following the instructions [here](https://github.com/spantaleev/matrix-docker-ansible-deploy/blob/master/docs/installing.md#4-finalize-the-installation).
* [Things to do next](https://github.com/spantaleev/matrix-docker-ansible-deploy/blob/master/docs/installing.md#5-things-to-do-next).
* The terraform state is stored in `terraform.tfstate` and `terraform.tfstate.backup` in the top-level directory of this repository. These files are not stored in this git repository and hence should be backed up separately. It is also possible to use any other backends supported by Terraform.

License
-------
This project is licensed under the GNU Affero General Public License v3.0 or later.
