terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.32.2"
    }
    namecheap = {
      source  = "namecheap/namecheap"
      version = "2.0.2"
    }

  }
}

provider "hcloud" {
  token = var.hcloud_token
}


provider "namecheap" {
  user_name = var.namecheap_user_name
  api_user  = var.namecheap_user_name
  api_key   = var.namecheap_api_key
}
