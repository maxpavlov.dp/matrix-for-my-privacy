resource "null_resource" "ansible" {
  depends_on = [hcloud_floating_ip_assignment.matrix]

  provisioner "local-exec" {
    command = "scripts/setup_ansible_matrix_config.sh ${var.namecheap_matrix_base_domain} ${hcloud_floating_ip.matrix.ip_address}"
  }

  provisioner "local-exec" {
    command = "cd floating-ip-setup && ../venv/bin/ansible-playbook -i '${hcloud_server.matrix.ipv4_address},' --user root -e 'floating_ip_address=${hcloud_floating_ip.matrix.ip_address}' playbook.yml"
  }

  provisioner "local-exec" {
    command = "cd matrix-docker-ansible-deploy && ../venv/bin/ansible-playbook -i inventory/hosts setup.yml --tags=setup-all && cd ../"
  }

  provisioner "local-exec" {
    command = "cd matrix-docker-ansible-deploy && ../venv/bin/ansible-playbook -i inventory/hosts setup.yml --tags=start && cd ../"
  }

}
